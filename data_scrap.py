
from bs4 import BeautifulSoup
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.options import Options  
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import pandas as pd
import time
url = 'https://idahostars.org/Families/page13735/1/size13735/12?page13735=1&size13735=48#dnn_ctr13735_Main_app'
# driver = webdriver.Chrome(ChromeDriverManager().install())
# driver.get(url)
data = []
def launchBrowser():
       chrome_options = Options()
       chrome_options.binary_location="../Google Chrome"
       chrome_options.add_argument("start-maximized")
       driver = webdriver.Chrome(ChromeDriverManager().install())

       driver.get(url)
       return driver
driver = launchBrowser()
time.sleep(5)
data = []
for x in range(0, 13):
    time.sleep(5)
    if x == 0:
        provider_name =  driver.find_elements(By.XPATH, '//*[@id="angrid13735"]/div[3]/div[3]/div/table/tbody/tr/td[1]/span')
        address =  driver.find_elements(By.XPATH, '//*[@id="angrid13735"]/div[3]/div[3]/div/table/tbody/tr/td[2]/span')
        city =  driver.find_elements(By.XPATH, '//*[@id="angrid13735"]/div[3]/div[3]/div/table/tbody/tr/td[3]/span')
        postal_code =  driver.find_elements(By.XPATH, '//*[@id="angrid13735"]/div[3]/div[3]/div/table/tbody/tr/td[4]/span')
        for i in range(len(provider_name)):
            tmp_data = {
                'Provider Name': provider_name[i].text,
                'address': address[i].text,
                'city': city[i].text,
                'Postal Code': postal_code[i].text,
            }
            data.append(tmp_data)
        print('================================')
        print(x)        
    else:
        time.sleep(6)
        provider_name =  driver.find_elements(By.XPATH, "//*[@id='angrid13735']/div[3]/div[3]/div/table/tbody/tr/td[1]/span")
        address =  driver.find_elements(By.XPATH, "//*[@id='angrid13735']/div[3]/div[3]/div/table/tbody/tr/td[2]/span")
        city =  driver.find_elements(By.XPATH, "//*[@id='angrid13735']/div[3]/div[3]/div/table/tbody/tr/td[3]/span")
        postal_code =  driver.find_elements(By.XPATH,"//*[@id='angrid13735']/div[3]/div[3]/div/table/tbody/tr/td[4]/span")
        for i in range(len(provider_name)):
            tmp_data = {
                'Provider Name': provider_name[i].text,
                'address': address[i].text,
                'city': city[i].text,
                'Postal Code': postal_code[i].text,
            }
            data.append(tmp_data)
        print('================================')
        print(x)
        print(pd.DataFrame(data))
    time.sleep(15)

    if x == 12:
        print('xlsx')
        pd.DataFrame(data).to_excel('childCareData.xlsx')
    driver.find_element(By.XPATH, '//*[@id="angrid13735"]/div[3]/div[4]/div/div[1]/ul/li[3]/button').click()
    time.sleep(1)
    print('clicked')
   

# time.sleep(5)
# df_data = pd.DataFrame(data)
# print(df_data)
# df_data.to_excel('childCareData.xlsx')